#
#	little script, that checks the last windows server backup
#	(c) 2017 Ing. Martin Strasser // www.msnetworks.eu
#

$lastbackup = $(Get-WBSummary).LastSuccessfulBackupTime
$now = get-date

$backupdiffok = [timespan]::fromhours(25)
$backupdiffwarn = [timespan]::fromhours(49)

if ($($now - $lastbackup) -lt $backupdiffok) {
	write-host "0 Windows_Backup – OK – Last Backup from $lastbackup"
}
elseif ($($now - $lastbackup) -lt $backupdiffwarn){
	write-host "2 Windows_Backup – WARNING – Last Backup from $lastbackup"
}
else {
	write-host "1 Windows_Backup – CRITICAL – Last Backup from $lastbackup"
}
